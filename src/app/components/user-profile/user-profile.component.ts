import { Component, OnInit } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';

@Component({
  selector: 'app-user-profile',
  template: `
  <section *ngIf="auth.user$ | async as user">
  <button mat-button [matMenuTriggerFor]="menu"><mat-icon>account_circle </mat-icon></button>
  <mat-menu #menu="matMenu">
    <button mat-menu-item>{{user.name}}</button>
  </mat-menu>
  </section>`
})
export class UserProfileComponent implements OnInit {

  constructor(public auth: AuthService) {}

  ngOnInit(): void {
  }

}
