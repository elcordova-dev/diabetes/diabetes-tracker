import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { first, take } from 'rxjs/operators';
import { DiabetesService } from 'src/app/services/diabetes.service';

@Component({
  selector: 'app-diabetes-form',
  templateUrl: './diabetes-form.component.html',
  styleUrls: ['./diabetes-form.component.scss']
})
export class DiabetesFormComponent implements OnInit, OnDestroy {
  diabetesForm = this.fb.group({
    id: null,
    typeDateRegister: 'today',
    sugarValue: [null, Validators.required],
    date: [null, Validators.required],
    hourOfDay:['am', Validators.required],
  });
  registeredId?:string;

  hasUnitNumber = false;
  serviceSubscribe: Subscription = new Subscription();

  constructor(private fb: FormBuilder, private diabetesService: DiabetesService, private _snackBar: MatSnackBar, private route: ActivatedRoute, private router: Router ) {}
  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    this.registeredId = <string>routeParams.get('id');

    if (this.registeredId) {
      this.diabetesService.searchByHash(this.registeredId).pipe(take(1)).subscribe(result=>{
        const firstVal = result[0];
        this.diabetesForm.patchValue({
          'id': firstVal.id,
          'date': firstVal.date,
          'sugarValue': firstVal.sugarValue,
          'hourOfDay': firstVal.hourOfDay,
          'typeDateRegister': 'not-today'
        })
      })
    }
  }

  onSubmit(): void {

    if (this.registeredId) {
      this.serviceSubscribe = this.diabetesService.update(this.diabetesForm.value).subscribe(value=>{
        this._snackBar.open('Registro Actualizado');
        this.diabetesForm.reset();
        this.router.navigate(['/']);
      },(error)=>{
        this._snackBar.open('Error al intentar actualizar el registro', 'Error');
      });
    } else {
      this.serviceSubscribe = this.diabetesService.save(this.diabetesForm.value).subscribe(value=>{
        this._snackBar.open('Registro guardado');
        this.diabetesForm.reset();
        this.router.navigate(['/']);
      },(error)=>{
        this._snackBar.open('Error al intentar guardar el registro', 'Error');
      });
    }
  }


  ngOnDestroy(): void {
    this.serviceSubscribe.unsubscribe();
  }
}
