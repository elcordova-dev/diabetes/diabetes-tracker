import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { map } from 'rxjs/operators';
import { DiabetesService } from '../services/diabetes.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  displayedColumns = ['hourOfDay','date', 'sugarValue', 'actions'];
  dataSource?: MatTableDataSource<RegisteredValue>;
  lastElementDeletet?: string;
  @ViewChild(MatPaginator) paginator?: MatPaginator;

  @ViewChild('modal') dialogTemplate?: TemplateRef<any>;

  constructor(private diabetesService: DiabetesService, readonly dialog: MatDialog) {
  }
  ngOnInit(): void {
    this.loadData()
  }


  handleDelete(element: any){
    this.lastElementDeletet = element.id;
    this.dialog.open(<any>this.dialogTemplate);
  }

  private loadData(){
    this.diabetesService.getAll().pipe(map((values: RegisteredValue[])=>{
      return values.map(val=>{
        return {...val, date: val.date?val.date:val.__createdtime__}
      });
    })).subscribe(values=>{
      this.dataSource = new MatTableDataSource<RegisteredValue>(values);
      this.dataSource.paginator = <MatPaginator>this.paginator;
    })
  }

  onDeleteConfirm(){
    this.diabetesService.delete(<string>this.lastElementDeletet).subscribe(deletedRow=>{
      this.loadData();
    });
    this.dialog.closeAll();
  }

  onDeleteCancel(){
    this.dialog.closeAll();
  }

}

export interface RegisteredValue {
  id:string,
  date?: Date;
  hourOfDay: string;
  sugarValue: number;
  typeDateRegister: string;
  __createdtime__?:Date;
  __updatedtime__?: Date;
}
