import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppConfigService } from './config.service';
import { environment } from 'src/environments/environment';

@Injectable()
export class DiabetesService {
  currentUser:any;
  config = this.configService.getConfig();
  table = environment.HARPERDB_TABLE;
  schema = environment.HARPERDB_SCHEMA;
  instance = environment.HARPERDB_INSTANCE;

  delete(lastElementDeletet: string ) {
    let raw = JSON.stringify({
      "operation":"delete",
      "schema" :  this.schema,
      "table": this.table,
      "hash_values":[lastElementDeletet]
    });
    return this.http.post(this.instance, raw, {headers: this.myHeaders})

  }

  myHeaders: HttpHeaders = new HttpHeaders(
    {
      "Content-Type":"application/json",
      "Authorization": `Basic ${environment.HARPERDB_KEY}`
    }
  )

  constructor(private http: HttpClient, private configService: AppConfigService) {
  }

  save(diabetesForm: any):Observable<any> {
    const record =  {
      ...diabetesForm,
      userId: this.config.sub,
      date: diabetesForm.date?diabetesForm.date:new Date()
    }
    let raw = JSON.stringify({
        "operation":"insert",
        "schema" :  this.schema,
        "table": this.table,
        "records": [
          record
        ]
    });
    return this.http.post(this.instance, raw, {headers: this.myHeaders})
  }

  getAll(): Observable<any>{

      let raw = JSON.stringify({
        "operation": "sql",
        "sql":`SELECT * FROM ${this.schema}.${this.table} where userId='${this.config?.sub}' order by date desc`
      });

      return this.http.post(this.instance, raw, {headers: this.myHeaders})

  }

  update(diabetesForm: any):Observable<any> {
    let raw = JSON.stringify({
        "operation":"update",
        "schema" :  this.schema,
        "table": this.table,
        "records": [
          {
            ...diabetesForm
          }
        ]
    });
    return this.http.post(this.instance, raw, {headers: this.myHeaders})
  }

  searchByHash(hash:string):Observable<any> {
    let raw = JSON.stringify({
      "operation":"search_by_hash",
      "schema" :  this.schema,
      "table": this.table,
      "hash_values":[hash],
      "get_attributes":["id","date","hourOfDay", "sugarValue", "typeDateRegister"]
    });
    return this.http.post(this.instance, raw, {headers: this.myHeaders});
  }
}
