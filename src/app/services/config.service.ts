import { Injectable } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';

@Injectable()
export class AppConfigService {
  private appConfig: any;

  constructor(public auth: AuthService) { }

  loadAppConfig() {
    return new Promise((resolve, reject) => {
      this.auth.user$
      .subscribe(data => {
        this.appConfig = data;
        resolve(data);
      });
    });
  }

  getConfig() {
    return this.appConfig;
  }
}
