import { Component, OnInit } from '@angular/core';
import { ChartType } from 'angular-google-charts';
import { DiabetesService } from '../services/diabetes.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-statistics-page',
  templateUrl: './statistics-page.component.html',
  styleUrls: ['./statistics-page.component.scss']
})
export class StatisticsPageComponent implements OnInit {

  myData:any;
  charType:ChartType = ChartType.Bar;
  loading = true;
  // chartColumns = ['Dias', ['Medicion', 'Hora'] ];
  chartColumns = [
    {label: 'Dias', type: 'date'},
    {label: ' Dia ', type: 'number'},
    {label: ' Noche ', type: 'number'}
  ];
  myOptions = {
    chart: {
      title: 'Mediciones de Glucosa / Días'
    },
    colors: ['#e6df44', '#063852'],
  }
  constructor(private diabetesService: DiabetesService, private datePipe: DatePipe) { }

  ngOnInit(): void {
    // this.datePipe.transform(date, 'd/MM/yy, h:mm a')
    this.diabetesService.getAll().subscribe((respData: any)=>{
      this.myData = respData.reduce((ant: any, act: any)=>{
        const date:string = <string>this.datePipe.transform(act.date, 'dd/MM/yyyy');
        let previousDate = ant[date];
        if (previousDate) {
          ant[date] = {...previousDate, [act['hourOfDay']] :  act['sugarValue'] };
        } else{
          previousDate = {[date]: {[act['hourOfDay']] :  act['sugarValue']}};
          ant =  {...ant, ...previousDate };
        }
        return ant;
      }, {});
      const keysDates= Object.keys(this.myData);
      this.myData = keysDates.map((key) => {
        let match = <RegExpExecArray>/(\d+)\/(\d+)\/(\d+)/.exec(key)
        let start_date = new Date(<string>(match[3])+"/"+<string>(match[2])+"/"+<string>(match[1]));
        return {'c':[{v:start_date, f:key}, {v:this.myData[key]['am']}, {v:this.myData[key]['pm']}]};
      });
    });
  }

}
