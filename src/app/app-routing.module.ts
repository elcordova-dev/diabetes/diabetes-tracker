import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@auth0/auth0-angular';
import { DiabetesFormComponent } from './components/diabetes-form/diabetes-form.component';
import { HomePageComponent } from './home-page/home-page.component';
import { StatisticsPageComponent } from './statistics-page/statistics-page.component';

const routes: Routes = [
  {path: '', component: HomePageComponent, canActivate:[AuthGuard]},
  {path: 'new', component: DiabetesFormComponent, canActivate:[AuthGuard]},
  {path: 'update/:id', component: DiabetesFormComponent, canActivate:[AuthGuard]},
  {path: 'statistics', component: StatisticsPageComponent, canActivate:[AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
