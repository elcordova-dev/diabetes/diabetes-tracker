const { writeFile } = require('fs');
const { argv } = require('yargs');
const fetch_node = require('node-fetch');
// read environment variables from .env file
require('dotenv').config();
// read the command line arguments passed with yargs
const environment = argv.environment;
const isProduction = environment === 'prod';
const targetPath = isProduction
   ? `./src/environments/environment.prod.ts`
   : `./src/environments/environment.ts`;
// we have access to our environment variables
// in the process.env object thanks to dotenv

if (!process.env.OAUTH_CLIENT_ID  ||
    !process.env.OAUTH_DOMAIN     ||
    !process.env.HARPERDB_KEY     ||
    !process.env.HARPERDB_SCHEMA  ||
    !process.env.HARPERDB_TABLE   ||
    !process.env.HARPERDB_INSTANCE) {
  console.error('All the required environment variables were not provided!');
  process.exit(-1);
}



const environmentFileContent = `
export const environment = {
  production: ${isProduction},
  OAUTH_DOMAIN: "${process.env.OAUTH_DOMAIN}",
  OAUTH_CLIENT_ID: "${process.env.OAUTH_CLIENT_ID}",
  HARPERDB_KEY: "${process.env.HARPERDB_KEY}",
  HARPERDB_SCHEMA: "${process.env.HARPERDB_SCHEMA}",
  HARPERDB_TABLE: "${process.env.HARPERDB_TABLE}",
  HARPERDB_INSTANCE: "${process.env.HARPERDB_INSTANCE}",
};
`;
// write the content to the respective file
writeFile(targetPath, environmentFileContent, (err: any)=> {
   if (err) {
      console.log(err);
   }
   console.log(`Wrote variables to ${targetPath}`);
});


// configure environment


var myHeaders = {
  "Content-Type": "application/json",
  "Authorization": `Basic ${process.env.HARPERDB_KEY}`
}

var raw = JSON.stringify({
	"operation":"create_schema",
	"schema": process.env.HARPERDB_SCHEMA
});

var requestSchema = {
  'method': 'POST',
  'headers': myHeaders,
  'body': raw
};

var requestTable = {
  'method': 'POST',
  'url': process.env.HARPERDB_SCHEMA,
  'headers': myHeaders,
  body: JSON.stringify({
	"operation":"create_table",
	"schema":process.env.HARPERDB_SCHEMA,
	"table": process.env.HARPERDB_TABLE,
	"hash_attribute":"id"
  })
}


async function create_resources(){
  await fetch_node(process.env.HARPERDB_INSTANCE, requestSchema)
  .then((response:any) => response.text())
  .then((result:any) => console.log(result))
  .catch((error:any) => console.log('error', error));

  await fetch_node(process.env.HARPERDB_INSTANCE, requestTable)
  .then((response:any) => response.text())
  .then((result:any) => console.log(result))
  .catch((error:any) => console.log('error', error));
}

create_resources().then();
